#=========================#
#       savager.file      #
#   Author: S-3-14-2020   #
#=========================#
from .element import path
class file():
	''' This base class stores, manipulates, and outputs path objects to SVG files
	'''
	# string constants
	#__color = ("red", "orange", "yellow", "green", "blue", "purple", "red", "orange", "yellow", "green", "blue", "purple")	#for coloring
	
	def __init__(self,filename="out.svg", height=2048, width=None):
		'''
		Args:
			filename (string): Path to file to write
			height (uint): Height of svg in pixels
			width (uint): Width of svg in pixels (if omitted, defaults to height)
		'''
		self.paths=[]			# list of path objects
		self.filename=filename
		self.height=height
		if width==None:
			self.width=self.height
		else:
			self.width=width
		self._uid=0

	@property
	def center(self):
		return (self.width/2, self.height/2)
	@property
	def header(self):
		return f'<svg width="{self.width}" height="{self.height}" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n'
	@property
	def footer(self):
		return '</svg>'
	@property
	def uid(self):
		self._uid+=1
		return self._uid-1
	
	def __enter__(self):
		# XXX verify that file can be opened?
		return self#.paths
	def __exit__(self, type, value, traceback):
		self.write()

	def write(self):
		''' Writes all paths to file
		'''
		with open(self.filename,'w') as file:
			file.write(str(self))

	def add_path_gen(self,generator,**kwargs):
		self.paths.append(path(list(generator), **kwargs))
	def add_path(self,path):
		self.paths.append(path)
	def add_use(self,**kwargs):
		'''piggybacks on another path
		'''
		self.add_path(path(drawF="USE",**kwargs))
				
	def __str__(self):
		string = self.header
		for p in self.paths:
			string += '\t' + p.draw()
		string += self.footer
		return string
	__repr__=__str__


	def rot_arrange(self, generator,  n, origin=None):
		'''Arranges n paths around the origin with equal spacing
		'''
		if origin is None:
			origin=self.center
		ox,oy=origin
		gen_list=list(generator)	# XXX this is rather hacky, we should probably ask for a list rather than a gen, but gens are conveinent.
		for i in range(n):
			self.add_path_gen(gen_list, transform=f"rotate({360*i/n},{ox},{oy})")

	def rot_arrange_use(self, generator,  n, origin=None):
		'''Uses a base path and references that path to save filespace
		'''
		if origin is None:
			origin=self.center
		ox,oy=origin
		uid=self.uid
		self.add_path_gen(generator,id=uid)
		for i in range(1,n):
			self.add_use(id=uid, x=0, y=0, transform=f"rotate({360*i/n},{ox},{oy})")