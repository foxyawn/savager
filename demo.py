#=========================#
#         flower          #
#   Author: S-3-14-2020   #
#=========================#

# from SVG.SVG import *
import numpy as np	# for (pi, sin, cos, log, e, exp)

import savager

# math constants
PHI = (1+np.sqrt(5))/2
phi = PHI-1

#=================================================
def beta(r=PHI, n=4):
	'''This was bult to make the intersections line up nicely!!!! like straight lines coming from the center :)
	Basicly, this makes radius grow by ratio(r), n times per rotation.
	Args:
		r (float): The ratio of new/old radii
		n (int): The number of times per rotaion that r is multiplied.
	'''
	return n/2/np.pi*np.log(r)

def rt2xy(polar, origin=(0,0)):
	'''Converts polar coordiates to their cartesian equivalents
	Args:
		polar (float,float): tuple representing radius and Theta respectively.
		offset (float,float): tuple representing x,y origin respectively.
	Returns:
		cartesian (float, float): tuple representing x,y respectively
	'''
	r,t=polar
	ox,oy=origin
	return (r*np.cos(t)+ox, r*np.sin(t)+oy)

def spiral_gen(r_max, t0=0, chirality=1, B_n=1, origin=(0,0)):
	'''Generator for cartesian coordinates of a golden spiral
	Bugs:
		if B is such that this creates an inward spiral, this will not terminate
	Args:
		r_max (float): max distance from origin that will be generated (assumes an outward spiral)
		t0 (float): initial angle in radians
		chirality (float): 1=rotates to the right as going out, -1 rotates to the left as going out
		B_n (float): number of curves
		origin (tuple): cartesian coordinates of origin to spiral around
	'''
	t = 0			# theta
	r = np.e		# radius
	dt = 2*np.pi/13	# delta theta # starts getting iffy around rot/8
	B=beta(PHI,B_n)	# curve constant

	while r <= r_max:
		yield rt2xy( (r, t*chirality + t0), origin)
		t+=dt
		r = np.exp(B*t)
	return
#=============================================================


with savager.file("demo.svg") as svg:	# open file
	#---old---
	# n=5
	# for p in range(n):
	# 	svg.add_path_gen(spiral_gen(r_max=svg.height/2, t0=p*2*np.pi/n, chirality=1, B_n=n, origin=svg.center))
	# 	svg.paths[-1].controls+=[svg.center]
	# n=8
	# for p in range(n):
	# 	svg.add_path_gen(spiral_gen(svg.height/2, p*2*np.pi/n, -1, n, svg.center))
	# 	svg.paths[-1].controls+=[svg.center]
	
	#---new---
	# n=5
	# svg.rot_arrange(spiral_gen(r_max=svg.height/2, chirality=1, B_n=n, origin=svg.center),n)
	# n=8
	# svg.rot_arrange(spiral_gen(r_max=svg.height/2, chirality=-1, B_n=n, origin=svg.center),n)

	#---new_use--- # this replicates the original spiral several times to reduce the filesize
	n=5
	svg.rot_arrange_use(spiral_gen(r_max=svg.height/2, chirality=1, B_n=n, origin=svg.center),n)
	n=8
	svg.rot_arrange_use(spiral_gen(r_max=svg.height/2, chirality=-1, B_n=n, origin=svg.center),n)